# DOCKER-CHROME-VNC #

Docker container based on Debian Jessie with google chrome running daemonized, controllable via x11vnc on port 5900.

* Version v0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* build:  ./build-dev.sh
* run: ./run-chrome.sh

### Who do I talk to? ###
* irc.freenode.net max_at #docker