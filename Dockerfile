# Run Chrome in a container
#
# docker run -it \
#	--net host \ # may as well YOLO
#	--cpuset-cpus 0 \ # control the cpu
#	--memory 512mb \ # max memory it can use
#	-v /tmp/.X11-unix:/tmp/.X11-unix \ # mount the X11 socket
#	-e DISPLAY=unix$DISPLAY \
#	-v $HOME/Downloads:/root/Downloads \
#	-v $HOME/.config/google-chrome/:/data \ # if you want to save state
#	--device /dev/snd \ # so we have sound
#	--name chrome \
#	maxmarkus/chrome
#

# Base docker image
FROM debian:jessie
MAINTAINER Markus Edenhauser <markus@edenhauser.com>

#ADD  https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb /src/google-chrome-stable_current_amd64.deb
ADD  google-chrome-stable_current_amd64.deb /src/google-chrome-stable_current_amd64.deb


# Install Chromium
RUN mkdir -p /usr/share/icons/hicolor && \
	apt-get update && apt-get install -y \
	ca-certificates \
	gconf-service \
	hicolor-icon-theme \
	libappindicator1 \
	libcanberra-gtk-module \
	libcurl3 \
	libexif-dev \
	libgconf-2-4 \
	libgl1-mesa-dri \
	libgl1-mesa-glx \
	libnspr4 \
	libnss3 \
	libpango1.0-0 \
	libv4l-0 \
	libxss1 \
	libxtst6 \
	wget \
	net-tools \
	xdg-utils \
	lxde-core \
	lxterminal \
	nginx \
	xvfb \
	openssh-server pwgen sudo \
	x11vnc supervisor \
	--no-install-recommends && \
	dpkg -i '/src/google-chrome-stable_current_amd64.deb' && \
	rm -rf /var/lib/apt/lists/*
	#libasound2 \ xfce4 xfce4-goodies \
COPY local.conf /etc/fonts/local.conf


# Configure Supervisor 
RUN mkdir -p /var/log/supervisor && mkdir -p /etc/supervisor
RUN cat /etc/init.d/supervisor
ADD ./etc/ /etc/
RUN rm /etc/init.d/supervisord # remove obsolete copied service file
#RUN sudo chmod +x /etc/init.d/supervisord
RUN update-rc.d supervisor defaults
#RUN sudo service supervisord start

# Configure VNC Password
#VNCPASS=`pwgen -c -n -1 10` 
RUN VNCPASS=ubuntu && echo "VNC Password: $VNCPASS" && mkdir -p ~/.vnc && x11vnc -storepasswd ubuntu ~/.vnc/passwd

# Create a default user with sudo access
RUN useradd ubuntu --shell /bin/bash --create-home
RUN usermod -a -G sudo ubuntu
RUN echo "ALL ALL = (ALL) NOPASSWD: ALL" >> /etc/sudoers

# Default configuration
#ENV SCREEN_GEOMETRY "1440x900x24"
ENV SCREEN_GEOMETRY "1200x700x24"
ENV DISPLAY :1

COPY bin_vncserver /etc/init.d/vncserver
RUN chmod +x /etc/init.d/vncserver && update-rc.d vncserver defaults

WORKDIR /data

# Autorun chrome
ENTRYPOINT [ "/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf" ]

# VNC port
EXPOSE 5900